export type TreeNode = {
    val: number;
    left: TreeNode | null;
    right: TreeNode | null;
};

export function binaryGen(min: number, max: number, depth: number): TreeNode {
    if (depth === 0) return null;
    const val = Math.floor(Math.random() * (max - min + 1) + min);
    const node: TreeNode = {
        val,
        left: binaryGen(min, val, depth - 1),
        right: binaryGen(val, max, depth - 1),
    };
    return node;
}

export function simpleBinaryRepresentation(node: TreeNode): {} {
    if (node === null) return null;
    return {
        [node.val]: {
            ...simpleBinaryRepresentation(node.left),
            ...simpleBinaryRepresentation(node.right),
        },
    };
}
