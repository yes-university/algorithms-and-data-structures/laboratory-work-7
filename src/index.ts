import treeify, { TreeObject } from "treeify";
import { TreeNode, binaryGen, simpleBinaryRepresentation } from "./binaryGen";

const binaryTree = binaryGen(-100, 100, 5);

console.log(
    "Current binary tree:\n",
    treeify.asTree(
        simpleBinaryRepresentation(binaryTree) as unknown as TreeObject,
        true,
        true
    )
);

function findNegativeNodes(node: TreeNode): TreeNode | null {
    if (node === null) return null;
    if (node.val < 0) {
        return {
            val: node.val,
            left: findNegativeNodes(node.left),
            right: findNegativeNodes(node.right),
        };
    }
    return findNegativeNodes(node.left) || findNegativeNodes(node.right);
}

const negativeNodes = findNegativeNodes(binaryTree);

console.log(
    "Negative nodes:\n",
    treeify.asTree(
        simpleBinaryRepresentation(negativeNodes) as unknown as TreeObject,
        true,
        true
    )
);
